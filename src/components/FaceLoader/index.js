import React from 'react';
import PropTypes from 'prop-types';

import './FaceLoader.css';

const propTypes = {
  size: PropTypes.number,
};

const defaultProps = {
  size: 14,
};


const bolts = ['*', ':', '･', 'ﾟ', '✧'];

export default class FaceLoader extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      activeBolt: 0,
    };
  }
  componentWillMount() {
    this.interval = setInterval(() => {
      const { activeBolt } = this.state;
      this.setState({
        activeBolt: (activeBolt + 1) % bolts.length,
      });
    }, 75);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    const boltString = bolts.filter((bolt, i) => {
      return i <= this.state.activeBolt;
    }).join('');
    return (
      <span className="faceLoader">
        {`(ﾉ◕ヮ◕)ﾉ${boltString}`}
      </span>
    );
  }
}

FaceLoader.propTypes = propTypes;
FaceLoader.defaultProps = defaultProps;
