import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import BuyStar from './components/BuyStar';

import './Face.css';

const propTypes = {
  id: PropTypes.string.isRequired,
  faceString: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  height: PropTypes.number.isRequired,
  size: PropTypes.number.isRequired,
  isBig: PropTypes.bool.isRequired,
};

export class Face extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      b64: '',
    };
  }
  componentDidMount() {
    const b64 = unescape(this.face.outerHTML);
    this.setState({
      b64,
    });
  }
  render() {
    const { height, isBig, faceString } = this.props;
    return (
      <div className="face">
        <BuyStar b64={this.state.b64} />
        <svg
          className="face__text"
          height={`${height}px`}
          width={`${height}px`}
          viewBox={`0 0 ${height} ${height}`}
          preserveAspectRatio="xMidYMid meet"
          ref={(c) => { this.face = c }}
        >
          {isBig ?
            <text
              fontFamily="Courier New, Courier, monospace"
              textAnchor="middle"
              x="50%"
              y="50%"
              fontSize={this.props.size}
              textLength={height}
              lengthAdjust="spacingAndGlyphs"
            >
              {faceString}
            </text>
            :
            <text
              fontFamily="Courier New, Courier, monospace"
              textAnchor="middle"
              x="50%"
              y="50%"
              fontSize={this.props.size}
            >
              {this.props.faceString}
            </text>
          }
        </svg>
        <div className="face__price">{this.props.price}</div>
      </div>
    );
  }
}

Face.propTypes = propTypes;

const mapStateToProps = (state, ownProps) => {
  const face = state.products.data[ownProps.id];
  const dollars = face.price / 100;
  const price = dollars.toLocaleString(
    "en-US",
    { style: "currency", currency: "USD" },
  );
  return {
    faceString: face.face,
    price,
    size: face.size,
  };
}

export default connect(
  mapStateToProps
)(Face);
