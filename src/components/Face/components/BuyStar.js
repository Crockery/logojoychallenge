import React from 'react';
import PropTypes from 'prop-types';

import './BuyStar.css';

const propTypes = {
  b64: PropTypes.string.isRequired,
};

export default class BuyStar extends React.PureComponent {
  render() {
    return (
      <a
        href-lang="image/svg+xml"
        className="buyStar"
        href={`data:image/svg+xml;utf8,${this.props.b64}`}
        download="yourface.svg"
      >
        <div className="buyStar__text">BUY</div>
        <svg
          version="1.1"
          xmlns="http://www.w3.org/2000/svg"
          x="0px"
          y="0px"
          viewBox="0 0 291.728 291.728"
        >
          <g>
            <path
              fill="red"
              d={`M291.728,145.86l-39.489,28.52l19.949,44.439l-48.469,4.896l-4.896,48.479l-44.439-19.959
              l-28.52,39.489l-28.52-39.489l-44.439,19.959l-4.896-48.479l-48.469-4.896l19.949-44.439L0,145.86l39.489-28.511L19.53,72.909
              l48.479-4.896l4.905-48.479l44.43,19.959l28.52-39.489l28.52,39.489l44.439-19.959l4.887,48.479l48.479,4.896l-19.949,44.43
              C252.24,117.34,291.728,145.86,291.728,145.86z`}
            />
          </g>
        </svg>
      </a>
    )
  }
}

BuyStar.propTypes = propTypes;
