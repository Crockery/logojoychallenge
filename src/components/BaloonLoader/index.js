import React from 'react';
import PropTypes from 'prop-types';

import './BaloonLoader.css';

const characters = ['.', 'o', 'O', '@', '*'];

const propTypes = {
  size: PropTypes.number.isRequired,
};

const defaultProps = {
  size: 16,
};

export default class BaloonLoader extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      activeItem: 0
    }
  }

  componentWillMount() {
    this.interval = setInterval(() => {
      const { activeItem } = this.state;
      this.setState({
        activeItem: (activeItem + 1) % characters.length,
      });
    }, 250);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    const { activeItem } = this.state;
    return (
      <span
        className="baloonLoader"
        style={{
          fontSize: `${this.props.size}px`,
        }}
      >
        {characters[activeItem]}
      </span>
    )
  }
}

BaloonLoader.propTypes = propTypes;
BaloonLoader.defaultProps = defaultProps;
