import fetch from 'isomorphic-fetch';

const get = (url) => {
  const promise = new Promise((resolve, reject) => {
    fetch(url, {
      method: 'get',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    })
      .then((res) => {
        if (res.ok) {
          return res.json();
        }
        throw new Error(res.status);
      })
      .then(body => resolve(body))
      .catch(error => reject(error));
  });
  return promise;
}

const fetchUtils = {
  get,
};

export default fetchUtils;
