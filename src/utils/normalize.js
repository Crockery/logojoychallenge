export default function normalizeArray(arr, key = 'id') {
  const ids = [];
  const data = arr.reduce((prev, value) => {
    if (value && !value.hasOwnProperty(key)) {
      return prev;
    }
    ids.push(value[key]);
    return Object.assign({}, prev, {
      [value[key]]: value,
    });
  }, {});
  return {
    ids,
    data,
  };
}
