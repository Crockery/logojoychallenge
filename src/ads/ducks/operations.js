import fetch from '../../utils/fetch';
import * as actions from './actions';

export function getAd(size, id) {
  return dispatch => {
    return fetch.get(`/ad?id=${parseInt(id, 10)}&size=${size}`)
      .then(res => {
        dispatch(actions.receiveAd(res));
      });
  }
}
