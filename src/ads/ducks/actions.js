import * as types from './types';

export function receiveAd(ad) {
  return {
    type: types.RECEIVE_AD,
    payload: {
      ad,
    },
  };
}
