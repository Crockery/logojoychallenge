import * as types from './types';

const initialState = {
  ids: [],
  data: {},
};

export default function adsReducer(state = initialState, action) {
  switch (action.type) {
    case types.RECEIVE_AD: {
      const { ad } = action.payload;
      if (state.ids.includes(ad.id)) return state;
      return {
        ids: [
          ...state.ids,
          ad.id,
        ],
        data: {
          ...state.data,
          [ad.id]: ad,
        },
      };
    }
    default: return state;
  }
}