import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import classNames from 'classnames';

import { adOperations } from './';

import './Container.css';

const propTypes = {
  id: PropTypes.string.isRequired,
  getAd: PropTypes.func.isRequired,
  size: PropTypes.number.isRequired,
  url: PropTypes.string.isRequired,
};

export class Ad extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
    };
  }
  componentWillMount() {
    if (this.props.url === '') {
      this.props.getAd(this.props.size, this.props.id);
    }
  }
  render() {
    const { url } = this.props;
    const { isLoaded } = this.state;
    return (
      <div
        className={classNames('ad', {
          isEmpty: url === '',
        })}
        style={{
          height: `${this.props.size}px`,
          width: `${this.props.size}px`,
        }}
      >
        <img
          src={url}
          className={classNames('ad__image', {
            isLoaded,
          })}
          onLoad={() => this.setState({ isLoaded: true })}
          alt="Cat ad!"
        />
      </div>
    );
  }
}

Ad.propTypes = propTypes;

const mapStateToProps = (state, ownProps) => {
  const { data } = state.ads;
  const ad = data[ownProps.id];
  const url = ad ? ad.url : '';
  return {
    url,
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    getAd: bindActionCreators(adOperations.getAd, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Ad);