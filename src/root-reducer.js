import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux';

import productsReducer from './products';
import adsReducer from './ads';

export default combineReducers({
  router: routerReducer,
  products: productsReducer,
  ads: adsReducer,
});
