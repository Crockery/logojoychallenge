export const FILTER_OPTIONS = [
  {
    value: 'id',
    label: 'by id',
  },
  {
    value: 'size',
    label: 'by size',
  },
  {
    value: 'price',
    label: 'by price',
  },
];
