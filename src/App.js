import React from 'react';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import classNames from 'classnames';

import { productOperations } from './products';

import ProductsContainer from './products/Container';
import { BaloonLoader } from './components';

import './App.css';

export class App extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      showSale: true,
    };
  }
  componentWillMount() {
    if(!this.props.hasLoadedFaces) {
      this.props.fetchFaces();
    }
  }

  render() {
    const { hasLoadedFaces } = this.props;
    const { showSale } = this.state;
    return (
    <div className="App">
      <div
        className={classNames('App__loader', {
          isLoaded: this.props.hasLoadedFaces,
        })}
      >
        Loading!
        <BaloonLoader />
      </div>
      {hasLoadedFaces ?
        <div
          className={classNames('saleMessage', {
            showSale,
          })}
        >
          <span
            className="close"
            onClick={() => this.setState({ showSale: false })}
          >
            x
          </span>
          <div className="saleMessage__message">
            <div className="line">[̲̅$̲̅(̲̅ ͡° ͜ʖ ͡°̲̅)̲̅$̲̅] MEGA SALE [̲̅$̲̅(̲̅ ͡° ͜ʖ ͡°̲̅)̲̅$̲̅]</div>
            <div className="line">100% off ALL faces!</div>
          </div>
        </div>
        :
        null
      }
      {hasLoadedFaces ?
        <main>
          <Route exact path="/" component={ProductsContainer} />
        </main>
        :
        null
      }
    </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    hasLoadedFaces: state.products.ids.length !== 0,
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchFaces: bindActionCreators(productOperations.fetchFaces, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
