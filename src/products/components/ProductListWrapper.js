import React from 'react';
import { PropTypes } from 'prop-types';
import SizeMe from 'react-sizeme';

import ProductList from './ProductList';
import './ProductListWrapper.css';

const propTypes = {
  size: PropTypes.object.isRequired,
  handleScroll: PropTypes.func.isRequired,
};

export class ProductListWrapper extends React.Component {
  render() {
    return (
      <div className="productListWrapper">
        <ProductList
          height={this.props.size.height}
          width={this.props.size.width}
          handleScroll={this.props.handleScroll}
        />
      </div>
    );
  }
}

ProductListWrapper.propTypes = propTypes;

export default SizeMe({ refreshMode: 'debounce', monitorHeight: true })(ProductListWrapper);
