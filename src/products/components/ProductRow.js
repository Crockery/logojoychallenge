import React from 'react';
import PropTypes from 'prop-types';

import { Face } from '../../components';
import Ad from '../../ads/Container';

import './ProductRow.css';

const propTypes = {
  index: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  ids: PropTypes.string.isRequired,
  bigIds: PropTypes.string.isRequired,
};

export default class ProductRow extends React.PureComponent {
  render() {
    const { ids, height, bigIds } = this.props;
    return (
      <div className="productRow">
        {ids.split('/').map((id, i) => {
          if (id.indexOf('catAd') > -1) {
            return <Ad key={id} size={height} id={id.split('-')[1]} />;
          }
          return (
            <Face
              key={id}
              id={id}
              isBig={bigIds.split('/').includes(id)}
              height={this.props.height}
            />
          );
        })}
      </div>
    )
  }
}

ProductRow.propTypes = propTypes;
