import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import debounce from 'lodash/debounce';

import { List } from 'react-virtualized';

import { productSelectors } from '../';

import ProductRow from './ProductRow';
import { FaceLoader } from '../../components';

import './ProductList.css';

const propTypes = {
  rows: PropTypes.array.isRequired,
  isFetching: PropTypes.bool.isRequired,
  isAtMax: PropTypes.bool.isRequired,
  height: PropTypes.number.isRequired,
  width: PropTypes.number.isRequired,
  faceCount: PropTypes.number.isRequired,
};

export class ProductList extends React.PureComponent {
  constructor(props) {
    super(props);
    this.renderRow = this.renderRow.bind(this);
    this.handleResize = this.handleResize.bind(this);
    this.debounceResize = debounce(this.handleResize, 30);
  }

  componentWillMount() {
    window.addEventListener('resize', this.debounceResize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.debounceResize);
  }

  handleResize() {
    if (!this.list) return;
    this.list.recomputeRowHeights();
  }

  renderRow({
    index,
    key,
    style,
  }) {
    const { rows } = this.props;
    const row = rows[index];
    const getRow = (type) => {
      switch (type) {
        case 'loader': {
          return (
            <div
              key={row.type}
              style={{
                height: `${row.height}px`
              }}
              className="productRow__loading"
            >
              <div className="contents">
                Hold up, grabbing you more awesome faces!
                <FaceLoader />
              </div>
            </div>
          );
        }
        case 'spacer': {
          return (
            <div
              key={row.type} 
              className="spacer"
              style={{
                height: `${row.height}px`,
              }}
            />
          );
        }
        case 'end': {
          return (
            <div key={row.type} className="productRow__end">
              {`(>人<) We've run out of products to show you! ◔ ⌣ ◔`}
            </div>
          );
        }
        default: {
          return (
            <ProductRow
              index={index}
              height={row.height}
              ids={row.idString}
              bigIds={row.bigIdString}
            />
          );
        }
      }
    }
    return (
      <div key={row.idString || `${key}${row.type}`} style={style}>
        {getRow(row.type)}
      </div>
    );
  }

  renderEmptyList() {
    return (
      <div>
        Whoops! Looks like we're alllll sold out.
      </div>
    )
  }

  render() {
    const { height, isAtMax, isFetching, width, rows } = this.props;
    return (
      <List
        className="productList"
        ref={(c) => { this.list = c; }}
        overscanRowCount={5}
        isLoading={isFetching}
        isDone={isAtMax}
        renderProps={`rowsOf-${this.props.faceCount}`}
        height={height}
        width={width}
        rowHeight={({ index }) => rows[index].height}
        rowCount={rows.length}
        rowRenderer={this.renderRow}
        noRowsRenderer={this.renderEmptyList}
        onScroll={this.props.handleScroll}
      />
    );
  }
}

ProductList.propTypes = propTypes;

const mapStateToProps = (state, ownProps) => {
  const { isAtMax, isFetching } = state.products.ui;
  const rows = productSelectors.createProductRows(state, ownProps.width);
  return {
    faceCount: state.products.ids.length,
    isAtMax,
    isFetching,
    rows,
  };
}

export default connect(
  mapStateToProps,
)(ProductList);
