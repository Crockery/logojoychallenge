import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import classNames from 'classnames';

import { FILTER_OPTIONS } from '../../constants';

import { BaloonLoader } from '../../components';

import { productOperations } from '../';

import './ProductFilter.css';

const propTypes = {
  changeFilter: PropTypes.func.isRequired,
  activeSort: PropTypes.string.isRequired,
  isFetching: PropTypes.bool.isRequired,
};

export class ProductFilter extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
    };
  }
  render() {
    const { activeSort } = this.props;
    const { isOpen } = this.state;
    return (
      <div className="productFilter">
        <span className="productFilter__label">Filter:</span>
        <div
          className="productFilter__button"
          onClick={() => this.setState({ isOpen: !isOpen })}
        >
          <span>{FILTER_OPTIONS.find(option => option.value === activeSort).label}</span>
          {this.props.isFetching ?
            <BaloonLoader />
            :
            null
          }
          <ul
            className={classNames('productFilter__list', {
              isOpen: this.state.isOpen,
            })}
          >
            {FILTER_OPTIONS.filter(option => option.value !== activeSort)
            .map(option => {
              return (
                <li
                  key={option.value}
                  className="productFilter__option"
                  onClick={() => this.props.changeFilter(option.value)}
                >
                  {option.label}
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    );
  }
}

ProductFilter.propTypes = propTypes;

const mapStateToProps = (state) => {
  return {
    activeSort: state.products.ui.activeSort,
    isFetching: state.products.ui.isFetchingFilter,
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    changeFilter: bindActionCreators(productOperations.changeFilter, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductFilter);
