import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import throttle from 'lodash/throttle';
import PropTypes from 'prop-types';

import { productOperations } from './';

import ProductListWrapper from './components/ProductListWrapper';
import ProductFilter from './components/ProductFilter';

import './Container.css';

const propTypes = {
  isAtMax: PropTypes.bool.isRequired,
  isFetching: PropTypes.bool.isRequired,
  fetchFaces: PropTypes.func.isRequired,
};

export class ProductsContainer extends React.PureComponent {
  constructor(props) {
    super(props);
    this.handleScroll = this.handleScroll.bind(this);
    this.throttleScroll = throttle(this.handleScroll, 500);
  }

  handleScroll({ clientHeight, scrollHeight, scrollTop }) {
    const { isAtMax, isFetching } = this.props;
    
    const nearBottom = scrollTop + (3 * clientHeight) >= scrollHeight;
    
    // do not get more if you're not near the bottom, or there are no more faces to get
    if (!nearBottom || isAtMax) {
      return false;
    } else if (!isFetching) {
      this.props.fetchFaces();
    }
  }

  render() {
    return (
      <div className="products">
        <svg id="faceTester" style={{
          position: 'fixed',
          opacity: '0',
          top: '-200',
        }} />
        <div className="products__header">
          <h2>ASCII Faces</h2>
          <span>Basically EVERYONE wants them</span>
          <ProductFilter />
        </div>
        <ProductListWrapper
          handleScroll={this.handleScroll}
        />
      </div>
    );
  }
}

ProductsContainer.propTypes = propTypes;

const mapStateToProps = (state) => {
  const { isFetching, isAtMax } = state.products.ui;
  return {
    isAtMax,
    isFetching,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchFaces: bindActionCreators(productOperations.fetchFaces, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProductsContainer)