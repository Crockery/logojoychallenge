import uniq from 'lodash/uniq';
import sortBy from 'lodash/sortBy';

import * as types from './types';
import normalize from '../../utils/normalize';

const initialState = {
  ids: [],
  data: {},
  ui: {
    isFetching: false,
    isFetchingFilter: false,
    isAtMax: false,
    activeSort: 'id',
  }
};

export default function productsReducer(state = initialState, action) {
  switch (action.type) {
    case types.FETCHING_FACES: {
      return {
        ...state,
        ui: {
          ...state.ui,
          isFetching: true,
        },
      };
    }
    case types.RECEIVE_FACES: {
      const { faces } = action.payload;
      const fetchedFaces = normalize(faces);
      const newIds = uniq([...state.ids, ...fetchedFaces.ids]);
      const newData = {
        ...state.data,
        ...fetchedFaces.data,
      };

      const newFaces = normalize(sortBy(
        newIds.map(id => newData[id]),
        state.ui.activeSort
      ));
      return {
        ...newFaces,
        ui: {
          ...state.ui,
          isFetching: false,
          isFetchingFilter: false,
        },
      };
    }
    case types.FILTER_FACES: {
      const { filter } = action.payload;
      const faces = state.ids.map(id => state.data[id]);
      const newFaces = normalize(sortBy(
        faces,
        filter
      ));
      return {
        ...state,
        ...newFaces,
      };
    }
    
    case types.CHANGE_FILTER: {
      return {
        ...state,
        ui: {
          ...state.ui,
          activeSort: action.payload.filter,
        },
      };
    }

    case types.FETCHING_FILTER: {
      return {
        ...state,
        ui: {
          ...state.ui,
          isFetchingFilter: true,
        },
      };
    }

    case types.AT_MAX: {
      return {
        ...state,
        ui: {
          ...state.ui,
          isFetching: false,
          isFetchingFilter: false,
          isAtMax: true,
        },
      };
    }

    default: return state;
  }
}