import * as types from './types';

export function fetchingFaces() {
  return {
    type: types.FETCHING_FACES,
  }
}

export function receiveFaces(faces) {
  return {
    type: types.RECEIVE_FACES,
    payload: {
      faces,
    },
  };
}

export function changeFilter(filter) {
  return {
    type: types.CHANGE_FILTER,
    payload: {
      filter,
    },
  };
}

export function fetchingFilter() {
  return {
    type: types.FETCHING_FILTER,
  };
}

export function atMax() {
  return {
    type: types.AT_MAX,
  };
}

export function filterFaces(filter) {
  return {
    type: types.FILTER_FACES,
    payload: {
      filter,
    },
  };
}
