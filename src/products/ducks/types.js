export const FETCHING_FACES = 'products/FETCHING_FACES';
export const RECEIVE_FACES = 'products/RECEIVE_FACES';
export const CHANGE_FILTER = 'products/CHANGE_FILTER';
export const FETCHING_FILTER = 'products/FETCHING_FILTER';
export const AT_MAX = 'products/AT_MAX';
export const FILTER_FACES = 'products/FILTER_FACES';
