import * as actions from './actions';
import * as selectors from './selectors';
import fetch from '../../utils/fetch';

export function fetchFaces() {
  return (dispatch, getState) => {
    dispatch(actions.fetchingFaces());
    const state = getState();
    const faceString = selectors.getFacesString(state);
    return fetch.get(faceString)
      .then(res => {
        if (!res.length) {
          return dispatch(actions.atMax());
        }
        dispatch(actions.receiveFaces(res));
      })
      .catch(err => {
        console.log(err);
      });
  }
}

export function changeFilter(filter) {
  return (dispatch, getState) => {
    const state = getState();
    dispatch(actions.changeFilter(filter));
    if (state.products.ui.isAtMax) {
      // If we've already fetched all of the faces, dont fetch more
      return dispatch(actions.filterFaces(filter));
    } else {
      dispatch(actions.fetchingFilter());
      return dispatch(fetchFaces());
    }
  };
}
