export function getFacesString(state) {
  // Generate the server reuqest string based on the current state.
  const hasFaces = state.products.ids.length !== 0;
  const base = '/faces';
  // If we havent fetched anything, grab the endpoints default response.
  if (!hasFaces) return base;
  // Use 50 as the limit when fetching new rows, with skip being
  // equal to the number of faces we've already pulled.
  const skip = state.products.ids.length;
  return `${base}?limit=50&skip=${skip}`;
}

const getFaceWidth = (string, size) => {
  // Using a hidden SVG, get the rendered width of the face based on its size.
  const faceString = document.createTextNode(string);
  const svgElement = document.createElementNS("http://www.w3.org/2000/svg", "text");

  svgElement.setAttributeNS(null, 'font-family', 'Courier New, Courier, monospace');
  svgElement.setAttributeNS(null, 'font-size', size);
  svgElement.appendChild(faceString);
  const tester = document.getElementById('faceTester');
  tester.appendChild(svgElement);

  // Get the bounding box of the svg.
  const bbox = tester.getBBox();
  svgElement.parentNode.removeChild(svgElement);
  return bbox.width;
}

const getTopWidth = (ids, data) => {
  const widths = ids.map(id => {
    const face = data[id];
    return getFaceWidth(face.face, face.size);
  });
  return Math.max(...widths);
}

export const createProductRows = (state, width) => {
  // If there are no faces loaded, just return an empty array.
  const { ids, data } = state.products;
  if (!ids.length) return [];

  // Need these to check if we want to append a loading, or fetching row
  const { isAtMax, isFetching } = state.products.ui;

  const topWidth = getTopWidth(ids, data);
  const numFacesPerRow = Math.ceil(width / topWidth);
  const faceSize = width / numFacesPerRow;
  let rows = [];
  let rowIndex = 0;
  let faceCount = 0;
  let adCount = 1;
  let faceInRow = 0;
  ids.forEach((id, i) => {
    rows[rowIndex] = rows[rowIndex] || {
      ids: [],
      height: faceSize,
      bigIds: [],
    };

    faceCount += 1;
    faceInRow += 1;

    // If we're at the 20th face, add an ad to the row.
    if (faceCount === 20) {
      rows[rowIndex].ids.push(`catAd-${adCount % 16}`);
      faceCount = 0;
      adCount += 1;
    } else {
      const face = data[id];
      const faceWidth = getFaceWidth(face.face, face.size);

      if (faceWidth > faceSize) rows[rowIndex].bigIds.push(face.id);
      // Add the face id to the row.
      rows[rowIndex].ids.push(id);
    }

    const rowIsFull = faceInRow === numFacesPerRow;
    const isLastFace = i === (ids.length - 1);

    if (rowIsFull || isLastFace) {

      rows[rowIndex].idString = rows[rowIndex].ids.join('/') || '';
      rows[rowIndex].bigIdString = rows[rowIndex].bigIds.join('/') || '';
      // Reset the builder variables
      rowIndex += 1;
      faceInRow = 0;
    }
  });
  if (isAtMax) {
    rows.push({
      type: 'end',
      height: topWidth,
    });
  } else if (isFetching) {
    rows.push({
      type: 'loader',
      height: topWidth,
    });
  } else {
    // If there are faces, and we're not loading insert a spacer
    // to avoid popin while scrolling.
    rows.push({
      type: 'spacer',
      height: topWidth,
    });
  }
  return rows;
}
