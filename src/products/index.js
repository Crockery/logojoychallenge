import * as types from './ducks/types';
import * as operations from './ducks/operations';
import * as actions from './ducks/actions';
import * as selectors from './ducks/selectors';
import reducer from './ducks/reducer';

export { types as productTypes };
export { actions as productActions };
export { operations as productOperations };
export { selectors as productSelectors };
export default reducer;
