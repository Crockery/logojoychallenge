const express = require('express');
const generateFaces = require('./utils/generate-faces').generateFaces;

const app = express();

app.set('port', process.env.PORT || 3001);

app.get('/faces', (req, res) => {
  const { query } = req;
  const limit = parseInt(query.limit, 10) || 100;
  const skip = parseInt(query.skip, 10) || 0;
  if (skip >= 350) return res.json([]);
  const sort = query.sort || 'id'
  setTimeout(() => {
    res.json(generateFaces(limit, skip, sort));
  }, 100 + Math.floor(Math.random() * 3000));
});

app.get('/ad', (req, res) => {
  const { query } = req;
  const size = parseInt(Math.floor(query.size), 10);
  
  res.json({
    url: `http://placekitten.com/${size}/${size}?image=${query.id}`,
    id: query.id,
  });
});

app.listen(app.get("port"), () => {
  console.log(`Server running at: http://localhost:${app.get("port")}/`); // eslint-disable-line no-console
});