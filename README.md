Discount Ascii Warehouse
====

Info
----

### How do I start the app?

After installing via `yarn install` or `npm install`, use `yarn start` or `npm start` to run the app.

Features
----

- Products are displayed rendered in a grid, as close to their original size as possible.
- When the products are pulled in via the `/faces` enpoint, the app will build "rows" based
on their size.
- The size of the columns in each row is determined by taking the rendered size of the biggest face pulled into the app, and finding the closest width below it that will result in an even number of columns.
- As a result, some (but very few) faces will have a rendered size bigger than the calculated column size. In these cases the face is scaled down to fit the calculated column size.
- As you scroll the app will pull in more faces once you get close to the bottom.
- If you reach 350 faces (totally arbitrary number), the app will display a "no more faces" message.
- Faces can be filtered via price, size, or id (default).
- You can download an xml svg version of the face at the rendered size by clicking on its "buy" button.

Faces API
----

- Basic query looks like: `/faces`
- Response format is JSON.
- To get a larger results set use the `limit` parameter, eg: `/faces?limit=100`
- To paginate results use the `skip` parameter, eg: `/faces?limit=15&skip=30` (returns 15 results starting from the 30th).

Ads
----

- Basic query looks like `/ad?id=1&size=100`
- Basic response looks like `http://placekitten.com/{SIZE}/{SIZE}?image={ID}
- The ap will insert an ad after every 19 products.
- A user will never see the same ad twice in a row.

Tech
----

- The app comes with a redux store to handle faces, ads, and ui elements.
- React router is installed if the app ever needs to expand, but is not currently being used.
- The app utilizes the `react-virtualized` library, to prevent rendering faces that aren't actually being displayed on the screen.
- `thunk` is being used to handle asynchronous requests through redux.