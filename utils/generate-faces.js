const faces = require('cool-ascii-faces').faces;

function getRandomString() {
  return (Math.random()).toString(36).substr(2);
}

function getId(i) {
  return `${i}-${getRandomString()}`;
}

function getSize() {
  const minSize = 12;
  return minSize + Math.floor(Math.random() * 30);
}

function getPrice() {
  return Math.ceil(Math.random() * 1000);
}

function generateFaces(limit, skip) {
  return [...Array(limit)].map((item, i) => {
    const skippedIndex = i + skip;
    return {
      id: getId(skippedIndex),
      size: getSize(),
      price: getPrice(),
      face: faces[skippedIndex % faces.length],
      total: faces.length,
    };
  });
}

module.exports = {
  generateFaces,
  getPrice,
  getSize,
  getId,
}
